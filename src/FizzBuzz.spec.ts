import { expect } from 'chai'
import { FizzBuzz } from './FizzBuzz'

describe('FizzBuzz', () => {
  it('does nothing with non-special numbers', () => {
    const someNumber = 2

    const result = FizzBuzz.calculate(someNumber)

    expect(result).to.eq('2')
  })

  it('says "Fizz" for numbers divisibles by 3', () => {
    const someNumber = 3

    const result = FizzBuzz.calculate(someNumber)

    expect(result).to.eq('Fizz')
  })

  it('says "Buzz" for numbers divisibles by 15', () => {
    const someNumber = 5

    const result = FizzBuzz.calculate(someNumber)

    expect(result).to.eq('Buzz')
  })
})