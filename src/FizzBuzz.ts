export class FizzBuzz {
  static calculate(number: Number): String {
    if (number === 3) {
      return 'Fizz'
    }

    if (number === 5) {
      return 'Buzz'
    }

    return number.toString()
  }
}